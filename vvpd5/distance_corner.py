import math


def corner_distance(x1, y1, x2, y2, x3, y3, x4, y4):
    distance_from_top = math.sqrt((x3 - x1) ** 2 + (y3 - y1) ** 2)
    distance_from_bottom = math.sqrt((x4 - x2) ** 2 + (y4 - y2) ** 2)
    sum_of_distances = distance_from_top + distance_from_bottom
    return round(sum_of_distances, 2)
