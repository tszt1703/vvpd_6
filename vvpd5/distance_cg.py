import math


def cg_distance(x1, y1, x2, y2, x3, y3, x4, y4):
    x_center1 = (x1 + x2) / 2
    y_center1 = (y1 + y2) / 2
    x_center2 = (x3 + x4) / 2
    y_center2 = (y3 + y4) / 2
    distance = math.sqrt((x_center2 - x_center1) ** 2 + (y_center2 - y_center1) ** 2)
    return round(distance, 2)
