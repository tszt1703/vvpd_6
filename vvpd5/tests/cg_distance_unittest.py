from vvpd5.distance_cg import cg_distance
import unittest


coordinates = [
    ((1, 2, 3, 0, 3, 4, 5, 2), 2.83),
    ((-1, 5, 2, 9, 4, 3, 5, 9), 4.12),
    ((4, 9, 5, 6, 2, -7, 0, 7), 8.28),
    ((7, 3, -8, -1, 5, 4, 6, 1), 6.18),
    ((0, 0, 2, 7, 6, 4, 8, 3), 6.0),
    ((5, 5, -4, -6, 5, 3, 2, 1), 3.91),
]


class CgDistanceTest(unittest.TestCase):
    def test_cg_distance_uni(self):
        for tuple_of_coordinates, result in coordinates:
            with self.subTest(tuple_of_coordinates=tuple_of_coordinates, result=result):
                result = cg_distance(*tuple_of_coordinates)
                self.assertEqual(result, result)

    def test_cg_distance_uni_error(self):
        with self.assertRaises(TypeError):
            cg_distance('n', 'r', 'u', 'w', 'q', 'a', 'c', 'g')
