from vvpd5.distance_corner import corner_distance
import pytest


@pytest.mark.parametrize("tuple_of_coordinates, result", [
    ((1, 2, 3, 0, 3, 4, 5, 2), 5.66),
    ((-1, 5, 2, 9, 4, 3, 5, 9), 8.39),
    ((4, 9, 5, 6, 2, -7, 0, 7), 21.22),
    ((7, 3, -8, -1, 5, 4, 6, 1), 16.38),
    ((0, 0, 2, 7, 6, 4, 8, 3), 14.42),
    ((5, 5, -4, -6, 5, 3, 2, 1), 11.22),
])
def test_corner_distance(tuple_of_coordinates, result):
    assert corner_distance(*tuple_of_coordinates) == result


def test_corner_distance_error():
    with pytest.raises(TypeError):
        corner_distance('n', 'r', 'u', 'w', 'q', 'a', 'c', 'g')
